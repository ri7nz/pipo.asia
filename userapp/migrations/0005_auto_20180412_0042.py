# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userapp', '0004_auto_20180411_0722'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mybalance',
            name='user',
            field=models.ForeignKey(to='userapp.UserProfile', null=True),
        ),
    ]
