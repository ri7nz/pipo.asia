# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userapp', '0003_auto_20180411_0717'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mybalance',
            name='user',
            field=models.ForeignKey(blank=True, to='userapp.UserProfile', null=True),
        ),
    ]
