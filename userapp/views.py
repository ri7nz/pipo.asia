# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import View
from django.views.generic.base import TemplateView
#from django.contrib.auth.mixins import LoginRequiredMixin
from userapp.models import UserProfile
from django.middleware import csrf
from django.contrib.auth.models import User
from django.contrib import auth
import datetime
from django.core.urlresolvers import reverse



#Class Untuk Daftar  (Reistrasi)
class DaftarUser(View):
    template_name = 'daftar.html'

    def get(self, request, *args, **kwargs):
        return render_to_response(
            self.template_name,
            context_instance=RequestContext(request))    
 

    def post(self, request, *args, **kwargs):
        
        akses = request.POST.get('akses')
        if akses == "daftar":
            full_name = request.POST.get('nama')
            email = request.POST.get('email')
            phone = request.POST.get('phone')
            if not full_name:
                return HttpResponse('nofullname')

            password = request.POST.get('pass')
            first_name = full_name.split(" ")
            last_name = ""
            if len(first_name) >= 2:
                last_name = first_name[1]

            try:
                check_user_contain = User.objects.get(username=email)
                return HttpResponse('emailtaken')
            except:
                new_user = User.objects.create_user(username=email, password=password, email=email)
                new_user.first_name = first_name[0]
                new_user.last_name = last_name
                new_user.is_staff = False
                new_user.is_active = True
                new_user.save()

                init_user = UserProfile(
                full_name="%s" % full_name,
                email=email,
                user=new_user,
                role='2',
               )
                init_user.save()
                auth_obj = auth.authenticate(username=email, password=password)
                auth.login(request, auth_obj)
                return HttpResponse('successregister')
        elif akses == "gantipass":
            try:
                old_pass = request.POST.get("old_pass")
                tmpemail = request.POST.get("tmpemail")
                new_pass = request.POST.get("new_pass")
                if len(new_pass) <= 8:
                    return HttpResponse("passlen")

                get_user = User.objects.get(id=request.user.id)
                if new_pass and get_user.check_password(old_pass):
                    get_user.set_password(new_pass)
                    get_user.save()
                else:
                    return HttpResponse("passnotmatch")
                auth_obj = auth.authenticate(username=tmpemail, password=new_pass)
                auth.login(request, auth_obj)
            except Exception:
                return HttpResponse("false")
            return HttpResponse("success")
        
        else:
            pass
        
            
            
#function Untuk Log Out
def Logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('homepageview'))


#Class untuk Login User 
class LoginUser(View):
    template_name = "login.html"
    
    def get(self, request, *args, **kwargs):
        return render_to_response(
               self.template_name,
               context_instance=RequestContext(request))    

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('userprofile'))

           
        username = request.POST.get('username')
        password = request.POST.get('password')
        try:
            user_obj = auth.authenticate(username=username, password=password)
            if user_obj is not None:
                if user_obj.is_active:
                    auth.login(request, user_obj)
                else:
                    return HttpResponse('inactive')
        except Exception:
            return HttpResponse('failed_login')
        else:
            if request.user.is_authenticated():
                return HttpResponse('success_login')
            else:
                return HttpResponse('failed_login')
            
            
